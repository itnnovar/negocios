
Instalación
===========

Clone the repository:

```bash
$ git clone url...
```

Create and activate the virtual environment:

```bash
$ mkvirtualenv negocios
$ workon negocios
```

Install the requirements:

```bash
(negocios)$ pip install -r requirements_dev.txt

    
Create the database and the database user:

```bash
$ sudo -u postgres psql
postgres=# CREATE ROLE negocios LOGIN PASSWORD 'negocios_asdzxc' NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
postgres=# CREATE DATABASE negocios WITH OWNER = negocios;
```
    
You have to create the file "settings/local.py" with your local database credentials:

```python
from .base import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'negocios',
        'USER': 'negocios',
        'PASSWORD': 'negocios_asdzxc',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}
DEBUG = True

```
    
Initialize the database and set-up the Django environment:

```bash
Create folder "assets" in the root of the project
(negocios)$ ./manage.py collectstatic
(negocios)$ ./manage.py makemigrations
(negocios)$ ./manage.py migrate
(negocios)$ ./manage.py createsuperuser
(negocios)$ ./manage.py runserver
```

