from django.db import models
from apps.negocio.models import NegocioComidas


class AbstractPublicacion(models.Model):
    titulo = models.TextField()
    descripcion = models.TextField()
    fecha_creacion = models.DateField(auto_now_add=True, blank=True)
    visible = models.BooleanField(default=True)
    foto = models.ImageField(upload_to='fotos_publicaciones')
    recomendado = models.BooleanField(blank=True)

    class Meta:
        abstract = True


class PublicacionComida(AbstractPublicacion):
    negocio_comida = models.ForeignKey(NegocioComidas, null=False, blank=False, on_delete=models.CASCADE)
