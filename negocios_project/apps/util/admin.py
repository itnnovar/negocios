from django.contrib import admin

from . import models
from django.contrib.contenttypes.admin import GenericTabularInline


class TelefonoInline(GenericTabularInline):
    model = models.Telefono
    extra = 1
    suit_classes = 'suit-tab suit-tab-contacto'


class EmailInline(GenericTabularInline):
    model = models.Email
    extra = 1
    suit_classes = 'suit-tab suit-tab-contacto'


class RedSocialInline(GenericTabularInline):
    model = models.RedSocial
    extra = 1
    suit_classes = 'suit-tab suit-tab-contacto'


admin.site.register(models.Provincia)
admin.site.register(models.Localidad)
admin.site.register(models.Departamento)
