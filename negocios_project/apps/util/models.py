from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.core.validators import RegexValidator
from apps.core.models import Signature


class Provincia(models.Model):

    class Meta:
        ordering = ('nombre',)

    nombre = models.CharField(max_length=60,)

    def __str__(self):
        return self.nombre


class Departamento(models.Model):

    class Meta:
        ordering = ('nombre',)

    nombre = models.CharField(max_length=60)
    provincia = models.ForeignKey(Provincia, on_delete=models.CASCADE)

    def __str__(self):
        return self.nombre


class Localidad(models.Model):

    class Meta:
        ordering = ('nombre',)
        verbose_name_plural = 'Localidades'

    nombre = models.CharField(max_length=100)
    departamento = models.ForeignKey(Departamento, on_delete=models.CASCADE)
    cod_postal = models.IntegerField(null=True, blank=True, default=None)

    def __str__(self):
        return self.nombre


class AbstractDireccion(models.Model):

    class Meta:
        abstract = True

    nombre_dir = models.CharField(
        'Nombre de Calle',
        max_length=100, 
        null=True, blank=True
    )
    numero_dir = models.PositiveIntegerField(
        'Número de Calle',
        null=True, blank=True
    )

    provincia = models.ForeignKey(
        Provincia, 
        null=True, blank=True, 
        on_delete=models.CASCADE
    )
    departamento = models.ForeignKey(
        Departamento,
        null=True, blank=True, 
        on_delete=models.CASCADE
    )
    localidad = models.ForeignKey(
        Localidad,
        null=True, blank=True,
        on_delete=models.CASCADE
    )

    def __str__(self):
        return '{} {}'.format(self.nombre_dir, self.numero_dir)


class AbstractPersona(models.Model):

    class Meta:
        abstract = True

    SEXO = (
        ('F', 'Femenino'),
        ('M', 'Masculino'),
    )

    CIVIL = (
        ('CA', 'Casado/a'),
        ('CO', 'Comprometido/a'),
        ('DI', 'Divorciado/a'),
        ('SO', 'Soltero/a'),
        ('VI', 'Viudo/a'),
    )

    nombre = models.CharField('Nombre', max_length=100)
    apellido = models.CharField('Apellido', max_length=100)
    dni = models.CharField('DNI', max_length=8, unique=True)
    cuil = models.CharField('Cuil', max_length=13, unique=True)
    sexo = models.CharField(max_length=10, choices=SEXO)
    fecha_nacimiento = models.DateField('Fecha de nacimiento')

    @property
    def person_full_name(self):
        return '%s, %s' % (self.apellido, self.nombre)

    def __str__(self):
        return '%s' % (self.person_full_name)


class Telefono(models.Model):
    class Meta:
        verbose_name = 'Teléfono'
        verbose_name_plural = 'Teléfonos'

    TELEFONO = 'tel'
    CELULAR = 'cel'

    TIPO = (
        (TELEFONO, 'Teléfono Fijo'),
        (CELULAR, 'Celular'),
    )

    regex_telefono = RegexValidator(
        regex=r'^\+?1?\d{9,15}$',
        message="El número de teléfono debe ingresarse en el formato: '+999999999'. Se permiten hasta 15 dígitos."
    )

    tipo = models.CharField(max_length=3, choices=TIPO)
    numero = models.CharField('Número', validators=[regex_telefono], max_length=17)
    content_type = models.ForeignKey(
        ContentType,
        limit_choices_to={
            'model__in': ('usuario', 'negocio')
        },
        on_delete=models.CASCADE,
        null=True, blank=True
    )
    object_id = models.PositiveIntegerField(null=True, blank=True)
    content_object = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        return "%s -> %s - %s" % (self.content_object, self.get_tipo_display(), self.numero)


class Email(models.Model):
    class Meta:
        ordering = ('email',)
        verbose_name_plural = 'Emails'

    email = models.EmailField(max_length=60)

    content_type = models.ForeignKey(
        ContentType,
        limit_choices_to={
            'model__in': ('usuario', 'negocio')
        },
        on_delete=models.CASCADE,
        null=True, blank=True
    )
    object_id = models.PositiveIntegerField(null=True, blank=True)
    content_object = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        return "%s -> %s" % (self.content_object, self.email)


class RedSocial(models.Model):
    class Meta:
        verbose_name = 'Red Social'
        verbose_name_plural = 'Redes Sociales'

    TIPO = (
        ('WSP', 'Whatsapp'),
        ('FB', 'Facebook'),
        ('TWI', 'Twitter'),
        ('INST', 'Instagram'),
    )
    tipo = models.CharField(max_length=4, choices=TIPO)
    url_red_social = models.URLField(max_length=200)

    content_type = models.ForeignKey(
        ContentType,
        limit_choices_to={
            'model__in': ('usuario', 'negocio')
        },
        on_delete=models.CASCADE,
        null=True, blank=True
    )
    object_id = models.PositiveIntegerField(null=True, blank=True)
    content_object = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        return "%s -> %s - %s" % (self.content_object, self.get_tipo_display(), self.url_red_social)
