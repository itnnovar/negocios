from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
# from django.contrib.auth.models import PermissionsMixin
# from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import (
        BaseUserManager, AbstractBaseUser
    ) 
from django.utils.translation import ugettext_lazy as _
 
from django.contrib.auth.models import UserManager 
from django.utils import timezone
from apps.util.models import (
    AbstractDireccion,
    Telefono,
)
 

class UserManager(BaseUserManager):
    def create_user(self, email, nombre, password=None):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('El campo Email es obligatorio')
        if not password:
            raise ValueError('El campo Password es obligatorio')
        if not nombre:
            raise ValueError('El campo Nombre es obligatorio')
        user_obj = self.model(
            email=self.normalize_email(email),
            nombre=nombre,
        )

        user_obj.set_password(password)
        user_obj.save(using=self._db)
        return user_obj

    def create_staffuser(self, email, nombre, password=None):
        """
        Creates and saves a staff user with the given email and password.
        """
        user = self.create_user(
            email,
            nombre,
            password=password,
            is_staff = True
        )
        return user

    def create_superuser(self, email, nombre, password=None):
        """
        Creates and saves a superuser with the given email and password.
        """
        user = self.create_user(
            email,
            nombre,
            password=password,
        )
        user.staff = True
        user.admin = True
        user.save(using=self._db)
        return user


# class Usuario(AbstractBaseUser, PermissionsMixin):
class Usuario(AbstractBaseUser, AbstractDireccion):
    
    email = models.EmailField(max_length=255, unique=True)
    nombre = models.CharField(max_length=255)
    apellido = models.CharField(max_length=255, blank=True)
    telefonos = GenericRelation(Telefono)
    fecha_creacion = models.DateTimeField(_('date joined'), default=timezone.now)
    active = models.BooleanField(default=True)
    admin = models.BooleanField(default=False)
    staff = models.BooleanField(default=False)
    foto = models.ImageField(upload_to='foto_usuario/', null=True, blank=True)
 
    objects = UserManager()
 
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['nombre']
 
    class Meta:
        verbose_name = _('Usuario')
        verbose_name_plural = _('Usuarios')
 
    def get_full_name(self):
        '''
        Returns the first_name plus the last_name, with a space in between.
        '''
        full_name = '%s' % (self.nombre)
        return full_name.strip()
 
    def get_short_name(self):
        '''
        Returns the short name for the user.
        '''
        return self.nombre
 
    def email_user(self, subject, message, from_email=None, **kwargs):
        '''
        Sends an email to this User.
        '''
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        return self.staff

    @property
    def is_admin(self):
        "Is the user a admin member?"
        return self.admin

    @property
    def is_active(self):
        "Is the user active?"
        return self.active