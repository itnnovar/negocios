from rest_framework.serializers import ModelSerializer
from rest_framework import viewsets

from apps.negocio.models import NegocioComidas, MenuEspecifico

class NegocioComidasSerializer(ModelSerializer):
	class Meta:
		model = NegocioComidas
		fields = ['id', 'nombre', 'valoracion', 'delivery', 'latitud', 'longitud', 'usuario']
		


class MenuSerializer(ModelSerializer):
	class Meta:
		model = MenuEspecifico
		fields = ['id', 'nombre', 'menu_general', 'precio']