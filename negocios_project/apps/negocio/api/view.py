from rest_framework.views import APIView
from apps.negocio.models import NegocioComidas, MenuEspecifico
from apps.negocio.api.serializer import NegocioComidasSerializer, MenuSerializer
from rest_framework import viewsets

class NegocioComidasAPI(viewsets.ModelViewSet):
	serializer_class = NegocioComidasSerializer
	queryset = NegocioComidas.objects.all()

class MenuAPI(viewsets.ModelViewSet):
	serializer_class = MenuSerializer
	queryset = MenuEspecifico.objects.all()