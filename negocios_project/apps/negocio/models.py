from django.db import models

from apps.core.models import Signature
from apps.util.models import AbstractDireccion, Telefono, Email, RedSocial
from apps.usuario.models import Usuario
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType


class HorarioAtencion(models.Model):
    dia = (
        ('LU', 'Lunes'),
        ('MA', 'Martes'),
        ('MI', 'Miercoles'),
        ('JU', 'Jueves'),
        ('VI', 'Viernes'),
        ('SA', 'Sabado'),
        ('DO', 'Domingo'),
    )
    hora_apertura1 = models.DateField()
    hora_cierre1 = models.DateField()
    hora_apertura2 = models.DateField()
    hora_cierre2 = models.DateField()


class AbstractNegocio(AbstractDireccion):

    class Meta:
        abstract = True

    nombre = models.CharField('Nombre', max_length=100)
    descripcion = models.TextField('Descripción', blank=True)
    telefonos = GenericRelation(Telefono)
    emails = GenericRelation(Email)
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    valoracion = models.DecimalField(default=0, max_digits=2, decimal_places=1)
    foto_portada = models.ImageField(verbose_name='Foto de Portada', blank=True, null=True)

    # accede a los atributos con ubicacion.latitude y ubicacion.longitude
    latitud = models.FloatField(blank=True)
    longitud = models.FloatField(blank=True)

    # Redes sociales
    red_social = GenericRelation(RedSocial)

    # horario de atencion
    horario_atencion = GenericRelation(HorarioAtencion)

    # @property
    # def latitud(self):
    #     return GeopositionField.latitude
    #
    # @property
    # def longitud(self):
    #     return GeopositionField.longitude


class MenuGeneral(models.Model):
    class Meta:
        verbose_name = 'Menú General'
        verbose_name_plural = 'Menúes Generales'

    nombre = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return self.nombre


class MenuEspecifico(models.Model):
    nombre = models.CharField(max_length=100, blank=True, null=True)
    menu_general = models.ForeignKey(MenuGeneral, on_delete=models.CASCADE)
    descripcion = models.TextField('Descripción', blank=True, null=True)
    precio = models.DecimalField(default=0, max_digits=7, decimal_places=2)

    class Meta:
        verbose_name = 'Menú Especifico'
        verbose_name_plural = 'Menúes Especificos'

    content_type = models.ForeignKey(
        ContentType,
        limit_choices_to={
            'model__in': ('negocio')
        },
        on_delete=models.CASCADE,
        null=True, blank=True
    )
    object_id = models.PositiveIntegerField(null=True, blank=True)
    content_object = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        return "%s -> %s" % (self.content_object, self.nombre)


class NegocioComidas(AbstractNegocio, Signature):
    delivery = models.BooleanField()
    menu = GenericRelation(MenuEspecifico)

    class Meta:
        ordering = ('nombre',)
        verbose_name = ('Negocio Comida')
        verbose_name_plural = ('Negocios Comida')

    def __str__(self):
        return self.nombre

