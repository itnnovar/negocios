from django.contrib import admin
from django.contrib.contenttypes.admin import GenericTabularInline

from apps.util import admin as tabular

from .models import NegocioComidas, MenuEspecifico, MenuGeneral, HorarioAtencion


class MenuEspecificoInline(GenericTabularInline):
    model = MenuEspecifico
    extra = 1
    suit_classes = 'suit-tab suit-tab-contacto'


class NegocioComidasAdmin(admin.ModelAdmin):
    search_fields = ['nombre']
    readonly_fields = ('created_on', 'modified_on',)
    list_display = ('nombre',)
    list_filter = ('nombre',)
    inlines = (
        MenuEspecificoInline,
        tabular.TelefonoInline,
        tabular.EmailInline,
        tabular.RedSocialInline,
    )

    fieldsets = (
        (None,
         {'fields': ('nombre', 'descripcion', 'usuario', 'delivery', 'valoracion')}),
        ('Domicilio', {'fields': ('nombre_dir', 'numero_dir', 'localidad', 'departamento', 'provincia')}),
        ('Ubicacion', {'fields': ('ubicacion', )}),
    )

    search_fields = ('nombre',)
    ordering = ('nombre',)
    # filter_horizontal = ('menu',)

    # def get_latitud(self):
    #     return NegocioComidas.ubicacion.latitude


admin.site.register(NegocioComidas, NegocioComidasAdmin)

admin.site.register(MenuGeneral)

admin.site.register(MenuEspecifico)

admin.site.register(HorarioAtencion)