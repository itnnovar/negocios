from django.views.generic import TemplateView



class Home(TemplateView):
    template_name = 'index.html'

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     context['proyectos_espera'] = Proyecto.objects.filter(estado='EE').order_by('fecha_votacion')[:10]
    #     context['proyectos_votando'] = Proyecto.objects.filter(estado='VO').order_by('fecha_votacion')[:10]
    #     context['proyectos_finalizado'] = Proyecto.objects.filter(estado='FI').order_by('fecha_votacion')[:10]
    #     context['politicos'] = Politico.objects.all()
    #     return context